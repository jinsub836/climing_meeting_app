import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green,
        title: const Text(
            '등산 사랑회 소개', style: TextStyle(color: Colors.white70),),
      ), drawer: Drawer(),
      body: Container(  margin: EdgeInsets.only(left: 30,right: 30),
        child:SingleChildScrollView(
        child: Column(
          children: [
            Container( alignment: Alignment.centerLeft,  margin: EdgeInsets.only(top: 10),
              child:
            Text('소개', style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),),
            Container(
              margin: EdgeInsets.all(10),
              child: Image.asset('assets/IE000895193_STD.jpg',width: 450, height: 150,
                fit: BoxFit.fill ),),
            Container( alignment: Alignment.centerLeft,  margin: EdgeInsets.only(top: 10),
              child: Text('등산 사랑회',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),) ,),
            Container( alignment: Alignment.centerLeft,  margin: EdgeInsets.only(top: 20),
              child: Text('회비:10,000') ,),
            Container( alignment: Alignment.centerLeft,
              child: Text('매주 토요일 새벽 6시 출발') ,),
            Container( alignment: Alignment.centerLeft, margin: EdgeInsets.only(top: 20),
              child: Text('주요 맴버',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),),
            Container(
              child:
            Row(
              children: [
                Column(
                  children: [
                    Container(  margin: EdgeInsets.only(top: 10,right: 20),
                      child: Image.asset('assets/111771500.1.jpg',width: 100, height:120 ,
                          fit: BoxFit.cover)),
                    Container( alignment: Alignment.centerLeft,
                      child: Text('회장 홍길동'),),
                  ],),
                Column(
                  children: [
                    Container(  margin: EdgeInsets.only(top: 10),
                      child: Image.asset('assets/BF.32192303.1.jpg',width:100 ,height: 120,
                          fit: BoxFit.cover )),
                    Container(
                      child: Text('부회장 고길동'),),
                  ],
                )
              ],
            ),
            )
          ],
        ) ,
      ),),
    );
  }
}
